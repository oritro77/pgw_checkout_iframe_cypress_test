/// <reference types="Cypress" />

import * as checkout from '../../checkout_iframe/checkout'

describe("PGW checkout iframe v1.2.0 OTP input page", function(){

    before(function(){
        checkout
        .navigate(`${Cypress.env("merchantDemoBaseURL")}/frontend/checkout/version/1.2.0-beta`)

    })

    describe("PGW otp input happy path", function(){
        before(function(){
            checkout.initiatePayment()

            checkout.typeWalletAndPressEnter(Cypress.env("user").wallet)
        })

        it(`OTP input field should be auto focused, has placeholder bKash Verification Code
        maxlength 11, autocomplete off`, function(){
            checkout
            .getOtpInputForm()
            .should("be.focused")
            .and("have.attr", "placeholder", "bKash Verification Code")
            .and("have.attr", "maxlength", "6")
            .and("have.attr", "autocomplete", "off")
            .and("have.attr", "required")
        })

        it(`check if wallet is truncated`, function(){
            checkout
            .getPhoneNumber()
            .invoke("text")
            .then(function(truncatedText){
                let wallet = Cypress.env("user").wallet
                let expectedTruncated = checkout
                                .putMultipleSpaceInText(checkout.truncateText(wallet, 3, 3, "*"), Cypress.env("walletSpaceBetween"))
                cy.wrap(truncatedText).should("equal", expectedTruncated)    
            })
        })

        it("should have resend otp text present", function(){
            checkout
            .getResendOTPText()
            .invoke("text")
            .should("contain", "Didn't receive code? Resend code")    
        })

        it("should have resend otp button", function(){
            checkout
            .getResendOTPButton()
            .should("have.class", "textButton")
            .find("u")
            .invoke("text")
            .should("contain", "Resend code")    
        })

        it("OTP input field should be cleared after inputing more then one char and pressing backsapce", function(){
            checkout
            .getOtpInputForm()
            .type("123123{backspace}")

            checkout
            .getOtpInputForm()
            .should("have.value", "")
        })

        it("should be able to input otp and go to pin screen", function(){
            checkout.typeOtpAndPressEnter(Cypress.env("user").otp)
    
            checkout
            .getLoaderImage()
            .should('have.css',
                    'background-image',
                    `url("${Cypress.env("assetBaseURL")}/resources/img/processing_gif.svg")`)
            
            checkout.getiFrameBody().should("contain", "Enter PIN of your bKash Account number")    
        })

        after(function(){
            checkout.closeiFrame()
        })
    })
        
    describe("Checkout otp input resend button functionality", function(){
        before(function(){
            checkout.initiatePayment()

            checkout.typeWalletAndPressEnter(Cypress.env("user").wallet)
        })

        it("should show Code sent successfully after clicking on Resend Code", function(){
            checkout.clickOnResendOTPButton()
    
            checkout
            .getLoaderImage()
            .should('have.css',
                    'background-image',
                    `url("${Cypress.env("assetBaseURL")}/resources/img/processing_gif.svg")`)
            
            checkout.getiFrameBody().should("contain", "Code sent successfully")    
            
        })
    
        it("should show Resend Limit Exceeded after clicking on Resend Code three times", function(){
            checkout.clickOnResendOTPButton()
            
            checkout.getiFrameBody().should("contain", "Code sent successfully")    
    
            checkout.clickOnResendOTPButton()
    
            //checkout.getResendOTPText().should("have.attr", "style", "display: none")
            
            checkout.getiFrameBody().should("contain", "Resend Limit Exceeded")
            
        })
        
        it("should show error message for wrong OTP", function(){
            checkout.typeOtp("123454")
            
            checkout.getConfirmButton().click()
                
            checkout.getErrorMessage().should("have.text", "Wrong verification code")
                
            
        })

        after(function(){
            checkout.closeiFrame()
        })
    
    })

    describe("OTP timeout", function(){
        before(function(){
            checkout.initiatePayment()

            checkout.typeWalletAndPressEnter(Cypress.env("user").wallet)
        })

        it(`OTP verifiction should fail after two minutes due to timeout
         and submit button will be disabled`, function(){
            cy.wait(Cypress.env("OTPtimeout"))
            
            checkout.typeOtpAndPressEnter(Cypress.env("user").otp)
            
            checkout
            .getErrorMessage()
            .should("have.text", "OTP verification time expired")

            //checkout.getConfirmButton().should("be.disabled")
        })

        after(function(){
            checkout.closeiFrame()
        })
    })
})