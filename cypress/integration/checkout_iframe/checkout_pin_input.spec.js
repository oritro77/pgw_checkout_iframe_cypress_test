/// <reference types="Cypress" />

import * as checkout from '../../checkout_iframe/checkout'

describe("PGW checkout iframe v1.2.0 PIN input page", function(){
    before(function(){
        checkout
        .navigate(`${Cypress.env("merchantDemoBaseURL")}/frontend/checkout/version/1.2.0-beta`)

        checkout.initiatePayment()

        checkout.typeWalletAndPressEnter(Cypress.env("user").wallet)

        checkout.typeOtpAndPressEnter(Cypress.env("user").otp)
    })

    it(`check if wallet is truncated`, function(){
        checkout
        .getPhoneNumber()
        .invoke("text")
        .then(function(truncatedText){
            let wallet = Cypress.env("user").wallet
            let expectedTruncated = checkout
                            .putMultipleSpaceInText(checkout.truncateText(wallet, 3, 3, "*"), Cypress.env("walletSpaceBetween"))
            cy.wrap(truncatedText).should("equal", expectedTruncated)    
        })
    })

    it(`PIN input field should be auto focused, has placeholder Enter Pin,
        autocomplete off`, function(){
        checkout
        .getPinInputForm()
        .should("be.focused")
        .and("have.attr", "placeholder", "Enter PIN")
        .and("have.attr", "autocomplete", "off")
        .and("have.attr", "required")
    })

    it("PIN input field should be cleared after inputing more then one char and pressing backsapce", function(){
        checkout
        .getPinInputForm()
        .type("123123{backspace}")

        checkout
        .getPinInputForm()
        .should("have.value", "")
    })

    it("should show wrong pin message for wrong pin", function(){
        checkout
        .typePinAndPressEnter("12122")
            
        checkout.getErrorMessage().should("have.text", "Wrong PIN")
    })
    
    it("should be able to input pin and close the iframe", function(){
        checkout
        .typePinAndPressEnter(Cypress.env("user").otp)

        checkout
        .getLoaderImage()
        .should('have.css',
                'background-image',
                `url("${Cypress.env("assetBaseURL")}/resources/img/processing_gif.svg")`)
            
        checkout.getiFrameBody().should("not.exist", {timeout: 30000})
        
    })
})