/// <reference types="Cypress" />

import * as checkout from '../../checkout_iframe/checkout'

describe("PGW checkout iframe v1.2.0 WALLET input page", function(){
    before(function(){
        checkout
        .navigate(`${Cypress.env("merchantDemoBaseURL")}/frontend/checkout/version/1.2.0-beta`)

        checkout.initiatePayment()
    })

    it("should open pgw iframe", function(){
        let iframe = checkout.getiFrameDocument()
        iframe.should("exist")
        checkout.getiFrameBody()
        
    })

    it("should open the wallet input screen", function(){
        checkout
        .getiFrameBody()
        .should("contain", "Your bKash Account number")
    })

    it("should have bKash logo in the top", function(){
        checkout
        .getbKashLogo()
        .should('have.css',
                'background-image',
                `url("${Cypress.env("assetBaseURL")}/resources/img/bkash_payment.png")`)
    })

    it("should have merchant logo present", function(){
        checkout.getiFrameBody().find("#merchantLogo")
    })

    it("should have merchant name present", function(){
        checkout
        .getMerchantName()
        .should('have.text',`${Cypress.env("merchant1").name}`)
    })

    it("should have label invoice present", function(){
        checkout
        .getLabelInvoice()
        .should('have.text','Invoice:')
    })

    it("should have invoice number present", function(){
        checkout
        .getInvoiceNumber()
        .invoke("text")
        .should('match', /(\w+)(\d?)/)
    })

    it("should have BDT ৳ present", function(){
        checkout
        .getBDTSign()
        .contains('৳')
    })

    it("should have price present", function(){
        checkout
        .getAmount()
        .invoke('text')
        .should('match', /[0-9]+[.][0-9]{2}/)
    })

    it("should have confirmation and t&c present", function(){
        checkout
        .getConfirmationTC()
        .should('have.text', "By clicking on  Confirm, you are agreeing to the terms & conditions ")
    })

    it("should have t&c link present", function(){
        checkout
        .getTCLink()
        .should('have.attr', "href", `${Cypress.env("t&cURL")}`)
    })

    it("should have CONFIRM button present", function(){
        checkout
        .getConfirmButton()
        .should("have.text", "Confirm")
    })

    it("should have CLOSE button present", function(){
        checkout
        .getCloseButton()
        .should("have.text", "Close")
    })

    it("should show payment cancellation screen after CLOSE button is clicked", function(){
        checkout
        .getCloseButton()
        .click()
        
        checkout
        .getPaymentCancellationText()
        .should('have.text', 'Payment Cancellation')
    
        checkout
        .getPaymentCancellationInfoText()
        .should('have.text', 'Are you sure you want to cancel this payment?')
        
        checkout
        .getYesButton()
        .should("contain", "Yes")
                    
        checkout
        .getNoButton()
        .should("contain", "No")
        .click()
    })

    it("should be able to visit terms & conditions link", function(){
        checkout
        .getiFrameBody()
        .find("a")
        .invoke("attr", 'href')
        .then(function(href){
            cy
            .request('GET', href)
            .its('body')
            .should('contain', 'TERMS AND CONDITIONS OF USING CHECKOUT')
        })
    })

    it(`Wallet input field should be auto focused, has placeholder e.g 01XXXXXXXXX
        maxlength 11, autocomplete off`, function(){
        checkout
        .getWalletInputForm()
        .should("be.focused")
        .and("have.attr", "placeholder", "e.g 01XXXXXXXXX")
        .and("have.attr", "maxlength", "11")
        .and("have.attr", "autocomplete", "off")
        .and("have.attr", "required")
    })

    it("should have dialer icon in footer", function(){
        checkout
        .getDialerLogo()
        .should('have.css',
                'background-image',
                `url("${Cypress.env("assetBaseURL")}/resources/img/phone.png")`)
    })

    it("should have 16247 written beside dialer icon", function(){
        checkout
        .get16247()
        .should('contain', '16247')
    })

    it("should be able to input only 11 numbers", function(){
        checkout
        .getWalletInputForm()
        .type("!@#$%^&*()-+_=asdfghjkl;'/.,mnnbzxcvqwertyuiop\\`~/.,<>/?")
        .should("have.value", "")

        checkout
        .getWalletInputForm()
        .type("0123456789001234567890")
        .invoke("val")
        .should("have.length", 11)
        
        checkout
        .getWalletInputForm()
        .type("{selectall}{backspace}")

         
    })

    it("should be able to input bKash wallet and go to otp screen", function(){
        checkout
        .typeWalletAndPressEnter(Cypress.env("user").wallet)

        checkout.getLoaderImage()
        .should('have.css',
                'background-image',
                `url("${Cypress.env("assetBaseURL")}/resources/img/processing_gif.svg")`)

        
        checkout
        .getiFrameBody()
        .should("contain", "Enter verification code sent to ")    
    })

    after(function(){
        checkout
        .closeiFrame()
    })
})



    