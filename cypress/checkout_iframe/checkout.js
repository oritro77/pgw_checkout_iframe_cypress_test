/// <reference types="cypress" />

export function navigate(url){
    cy.visit(url)
}

export function initiatePayment(){
    cy.get("#new_amount").invoke("val").should('match', /[0-9]+[.][0-9]{2}/)
    cy.get('#bKash_button', {force: true}).click()
}

export function getiFrameDocument(){
    return cy.get('iframe').its('0.contentDocument').should('exist')
}

export function getiFrameBody(){
    return cy.get('iframe')
            .its('0.contentDocument.body')
            .should('not.be.empty')
            .then(cy.wrap)
}

export function getElementOfiFrame(element){
    return getiFrameBody().find(element)
}

export function getInputHolderFor(what){
    return getElementOfiFrame("#inputHolder span[for=" + what + "]")
}

export function getInputHolderForWallet(){
    return getInputHolderFor("wallet")
}

export function getInputHolderForOTP(){
    return getInputHolderFor("otp")
}

export function getInputHolderForPin(){
    return getInputHolderFor("pin")
}

export function getbKashLogo(){
    return getElementOfiFrame("#logo")
}

export function getMerchantLogo(){
    return getElementOfiFrame("#merchantLogo")
}

export function getMerchantName(){
    return getElementOfiFrame("#merchantName")
}

export function getLoaderDiv(){
    return getElementOfiFrame("#loader_div")
}

export function getLoaderImage(){
    return getElementOfiFrame("#loader_image")
}

export function getLabelInvoice(){
    return getElementOfiFrame(".infoInvoice span.invoiceText")
}

export function getInvoiceNumber(){
    return getElementOfiFrame("#merchantInvoice1")
}

export function getBDTSign(){
    return getElementOfiFrame(".trxMerchantAmount span.merchantAmount")
}

export function getAmount(){
    return getElementOfiFrame("#merchantAmountVal")
}

export function getConfirmationTC(){
    return getElementOfiFrame("#inputHolder > span:nth-child(3)")
}

export function getTCLink(){
    return getElementOfiFrame("a")
}

export function getDialerLogo(){
    return getElementOfiFrame("#dialer")
}

export function get16247(){
    return getElementOfiFrame("#dialText")
}

export function getPhoneNumber(){
    return getElementOfiFrame("#phone_num")
}

export function getResendOTPText(){
    return getElementOfiFrame("#resend_info_text")
}

export function getResendOTPButton(){
    return getElementOfiFrame("#resend_otp_text")
}

export function clickOnResendOTPButton(){
    return getResendOTPButton().click()
}

export function getErrorMessage(){
    return getElementOfiFrame("#error")
}

export function getConfirmButton(){
    return getElementOfiFrame("#submit_button")
}

export function getCloseButton(){
    return getElementOfiFrame("#close_button")
}

export function getPaymentCancellationText(){
    return getElementOfiFrame("#cancelTextHolder .infoTextHead")
}

export function getPaymentCancellationInfoText(){
    return getElementOfiFrame("#cancelTextHolder .infoText")
}

export function getYesButton(){
    return getElementOfiFrame("#yes_button")
}

export function getNoButton(){
    return getElementOfiFrame("#no_button")
}

export function getWalletInputForm(){
    return getElementOfiFrame("#wallet")
}

export function typeWalletAndPressEnter(wallet){
    return getWalletInputForm().type(wallet + "{enter}")
}

export function getOtpInputForm(){
    return getElementOfiFrame("#otp")
}

export function typeOtpAndPressEnter(otp){
    return getOtpInputForm().type(otp + "{enter}")
}

export function typeOtp(otp){
    return getOtpInputForm().type(otp)
}

export function getPinInputForm(){
    return getElementOfiFrame("#pin")
}

export function typePinAndPressEnter(pin){
    return getPinInputForm().type(pin + "{enter}")
}

export function closeiFrame(){
    getCloseButton()
    .click()
    
    getYesButton()
    .click()
}

export function truncateText(text, visibleFirst, visibleLast, replacer){
    let len = text.length
    return text.slice(0, visibleFirst) 
            + replacer.repeat(len - (visibleFirst + visibleLast)) 
            + text.slice(-visibleLast)
}

export function putMultipleSpaceInText(text, spaceBreakdown){
    
    let newText = ""
    let prevVal = 0
    for(let currentVal in spaceBreakdown){
        newText += text.slice(prevVal, prevVal + spaceBreakdown[currentVal]) + " "
        prevVal += spaceBreakdown[currentVal]
    }
    
    return newText.trim()
}



